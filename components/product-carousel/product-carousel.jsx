import styles from "./product-carousel.module.scss";
import { CarouselProvider, Slider, Slide } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import CustomDots from "./custom-dots";

const ProductCarousel = ({ images }) => {
  return (
    <CarouselProvider
      className={styles.productCarousel}
      naturalSlideWidth={1}
      naturalSlideHeight={1}
      totalSlides={images.length}
    >
      {/*   creates a slider and maps over fetched images to display them  */}
      <Slider className={styles.slider}>
        {images.map((image, index) => {
          return (
            <Slide index key={index}>
              <img alt="" src={image}></img>
            </Slide>
          );
        })}
      </Slider>
      <CustomDots />
    </CarouselProvider>
  );
};

export default ProductCarousel;
