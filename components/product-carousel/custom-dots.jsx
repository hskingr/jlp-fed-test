import { Dot } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import { CarouselContext } from "pure-react-carousel";
import { useState, useContext, useEffect } from "react";
import styles from "./custom-dots.module.scss";
// There was not any display that looked like the design to indicate
// the position of the images in the carousel, So I made a custom implementation
export default function CustomDots(props) {
  const carouselContext = useContext(CarouselContext);
  const [slideNum, setSlideNum] = useState(0);
  let { totalSlides } = carouselContext.state;
  totalSlides -= 1;

  useEffect(() => {
    function onChange() {
      setSlideNum(carouselContext.state.currentSlide);
    }
    carouselContext.subscribe(onChange);
    return () => carouselContext.unsubscribe(onChange);
  }, [carouselContext]);

  return (
    // The logic here determines what dot to highlight on the carousel
    // The styles change on the dot depending on the position of the images.
    <div>
      <Dot className={styles.dot} slide={0}>
        {slideNum === 0 ? (
          <span className={styles.active}>&#11044;</span>
        ) : (
          <span className={styles.inActive}>&#11044;</span>
        )}
      </Dot>
      <Dot
        className={styles.dot}
        slide={
          slideNum === 0
            ? 1
            : slideNum === totalSlides
            ? totalSlides - 1
            : slideNum
        }
      >
        {slideNum === 0 ? (
          <span className={styles.inActive}>&#11044;</span>
        ) : slideNum === totalSlides ? (
          <span className={styles.inActive}>&#11044;</span>
        ) : (
          <span className={styles.active}>&#11044;</span>
        )}
      </Dot>
      <Dot className={styles.dot} slide={totalSlides}>
        {slideNum === totalSlides ? (
          <span className={styles.active}>&#11044;</span>
        ) : (
          <span className={styles.inActive}>&#11044;</span>
        )}
      </Dot>
    </div>
  );
}
