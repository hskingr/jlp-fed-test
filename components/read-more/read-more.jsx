import DOMPurify from "isomorphic-dompurify";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./read-more.module.scss";
import Head from "next/head";
import Router from "next/dist/next-server/server/router";
import { useEffect, useState } from "react";
import Link from "next/link";

export default function ReadMore({ data }) {
  // check for click on readMore arrow button
  const [readMoreClicked, setReadMoreClicked] = useState(false);

  //UPDATE: send event to the button NOT A BOOLEAN VALUE
  const clicked = (event) => {
    event.preventDefault();
    setReadMoreClicked(true);
  };

  // matches the first <p> element and splits it with the rest of the content
  const matches = data.match(/^<p>[^]+?<\/p>$/gm);
  const firstParagraph = matches[0];
  // joins the rest of the matches together apart from the first <p>
  // This is so the ReadMore get displayed after a paragraph
  const theRest = matches.slice(1).join("");
  return (
    <div>
      <div
        className={styles.someText}
        dangerouslySetInnerHTML={{
          __html: firstParagraph,
        }}
      />
      {/* The button area disappears once clicked. Functionality to Read Less does not exist */}
      {!readMoreClicked && (
        <div className={styles.readMoreContainer}>
          <h4 className={styles.readMoreText}>Read More</h4>
          <button
            onClick={clicked}
            className={styles.arrowButton}
            aria-label="read more button"
          >
            <i />
          </button>
        </div>
      )}
      {readMoreClicked && (
        <div
          className={styles.readMore}
          dangerouslySetInnerHTML={{
            __html: theRest,
          }}
        ></div>
      )}
    </div>
  );
}
