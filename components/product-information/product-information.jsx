import React from "react";
import styles from "./product-information.module.scss";
import ReadMore from "../read-more/read-more";
import { useEffect, useState } from "react";
import DOMPurify from "isomorphic-dompurify";

const ProductInformation = ({ code, productInformation }) => {
  const [screenWidth, setScreenWidth] = useState();
  const handleResize = () => setScreenWidth(window.innerWidth);

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
  }, [handleResize]);

  // help prevent XSS attacks
  const sanitizedProductInfo = DOMPurify.sanitize(productInformation);

  return (
    <section className={styles.productInfo}>
      <h3>Product information</h3>
      {/* Product info is displayed depending on screen size */}
      {screenWidth >= 768 ? (
        <>
          <p className={styles.productInfoCode}> Product code: {code} </p>
          <ReadMore data={sanitizedProductInfo} />
        </>
      ) : (
        <>
          <div
            dangerouslySetInnerHTML={{
              __html: sanitizedProductInfo,
            }}
          />
          <p className={styles.productInfoCode}> Product code: {code} </p>
        </>
      )}
    </section>
  );
};

export default ProductInformation;
