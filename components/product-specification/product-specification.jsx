import React from "react";
import styles from "./product-specification.module.scss";

const ProductSpecification = ({ features }) => {
  return (
    <>
      <div className={styles.productInfoSpef}>
        <h3>Product specification</h3>
        <ul>
          {features[0].attributes.map((item, index) => (
            <li key={index}>
              <div className={styles.productSpefItem}>
                <div className={styles.productSpefItemName}>{item.name}</div>
                <div className={styles.productSpefItemValue}>{item.value}</div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default ProductSpecification;
