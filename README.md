# UI Dev Technical Test - Dishwasher App

# Notes

This task took a bit longer than I expected. I have a knowledge of React and how to use it, but have not used Next.js or Jest before. I attempted to learn how to implement Jest into Next.js and React, but struggled to get a good understanding of how to implement it in the time that I had. That said, I will be focusing on this gap in my knowledge to learn more about how to use Jest.

## 3rd party Code

I added the `pure-react-carousel` and `isomorphic-dompurify` to the code.

`pure-react-carousel` was added to provide a carousel functionality to the product item page. It would not have been a good use of time to build this from scratch. The package is actively maintained and has a lot of activity, so I was confident that it would suit my needs at this time.

`isomorphic-dompurify` was recommended as a package to prevent XSS attacks. It is one of the fastest implementations of DOM sanitizing and works both server and client side for Next.js

## Considerations

Testing is obviously the big thing I missed out on. On the other hand, attempting to use a TDD approach added more confusion to learning Next.js and Jest.

In reference to loading the images from the API call, I did see in the Next.js documentation that there was an Image component which claims to optimise the display of images to the client side. In the future, I would implement this change instead of using the `<img>` tag to see what performance gains there are.

I may not have made good use of distilling certain functionalities to their own components. This might have been due to working with a different file structure due to Next.js. In the future, I would break down some areas into more components. Specifically, the `[id].jsx` file. Product Information, Price Information and Product Specification would benefit from isolation from the product page and help make the code easier to understand.
