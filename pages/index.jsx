import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import { useState } from "react";

// export async function getServerSideProps() {
//   try {
//     const response = await fetch(
//       "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//     );
//     const data = await response.json();
//     return {
//       props: {
//         data: data,
//       },
//     };
//   } catch (error) {
//     return {
//       notFound: true,
//     };
//   }
// }

export async function getServerSideProps() {
  try {
    const fsPromises = require("fs").promises;
    const fileToRead = `${process.cwd()}/mockData/data.json`;
    const response = await fsPromises.readFile(
      fileToRead,
      async (err, data) => {
        return data;
      }
    );
    const data = await JSON.parse(response);
    return {
      props: {
        data: data,
      },
    };
    // if there is no data, the page returns a 404
  } catch (error) {
    return {
      notFound: true,
    };
  }
}

const Home = ({ data }) => {
  const items = data.products;
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <div className={styles.header}>
          <h1> Dishwashers {items ? `(${items.length})` : null}</h1>
        </div>
        <div className={styles.content}>
          {/* get the first 20 results */}
          {items.slice(0, 20).map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>
                <div className={styles.contentItem}>
                  <div className={styles.contentItemImage}>
                    <img src={item.image} alt="" style={{ width: "100%" }} />
                  </div>
                  <div>{item.title}</div>
                  <div className={styles.price}>{item.price.now}</div>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
