import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./[id].module.scss";
import Head from "next/head";
import { useEffect, useState } from "react";
import Link from "next/link";
import ProductInformation from "../../components/product-information/product-information";
import ProductSpecification from "../../components/product-specification/product-specification";

// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();

//   return {
//     props: { data },
//   };
// }

export async function getServerSideProps(context) {
  const id = context.params.id;
  const fsPromises = require("fs").promises;
  const response = await fsPromises.readFile(
    `${process.cwd()}/mockData/data2.json`,
    async (err, data) => {
      return data;
    }
  );

  //get the correct index of item
  const items = await JSON.parse(response).detailsData;
  const itemIndex = items.findIndex((item) => {
    return item.productId === id;
  });
  //returns an error page if the product does not exist
  if (itemIndex === -1) {
    return {
      notFound: true,
    };
  } else {
    return {
      props: {
        data: items[itemIndex],
      },
    };
  }
}

// determines what data to display depending on screen width
const ProductDetail = ({ data }) => {
  const {
    title,
    media,
    price,
    displaySpecialOffer,
    additionalServices,
    details: { features, productInformation },
    code,
  } = data;

  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div className={styles.header}>
        <Link href="/">
          <button className={styles.arrowButton} aria-label="back button">
            <i />
          </button>
        </Link>
        <h1>{title}</h1>
      </div>
      <div className={styles.container}>
        {/* component to display the images */}
        <ProductCarousel images={media.images.urls} />
        <div className={styles.priceInfo}>
          <h1>£{price.now}</h1>
          <div className={styles.priceInfoDetail}>
            <div>{displaySpecialOffer}</div>
            <div className={styles.guaranteeDetail}>
              {additionalServices.includedServices}
            </div>
          </div>
        </div>
        <ProductInformation
          code={code}
          productInformation={productInformation}
        />
        <ProductSpecification features={features} />
      </div>
    </div>
  );
};

export default ProductDetail;
