import "@testing-library/jest-dom/extend-expect";
// This is added because there was an issue with
// JSDOM/testing the DOMPurify package
import { TextDecoder, TextEncoder } from "util";

global.TextEncoder = TextEncoder;
global.TextDecoder = TextDecoder;
