import React from "react";
import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ProductDetail, {
  getServerSideProps,
} from "../pages/product-detail/[id].jsx";

async function setup() {
  const context = {
    params: {
      id: "3218074",
    },
  };
  const response = await getServerSideProps(context);
  const { container } = render(<ProductDetail data={response.props.data} />);
  return container;
}

test("product item page displays", async () => {
  //GIVEN
  // a user wants to access a specific item page
  const context = {
    params: {
      id: "3218074",
    },
  };
  const response = await getServerSideProps(context);
  //WHEN
  // the page loads
  const { container } = render(<ProductDetail data={response.props.data} />);
  //THEN
  // display the results
  const nameOfProduct = screen.getByRole("heading", {
    name: "Bosch Serie 2 SMS24AW01G Freestanding Dishwasher, White",
  });
  expect(nameOfProduct).toBeInTheDocument;
  cleanup();
});

/*
check if the readmore button exists if the screen width is
larger than 768px

*/
test("read more button appear if screen width is larger than 768px", async () => {
  global.window.innerWidth = 1024;
  const container = await setup();
  const buttonExists = screen.getByRole("button", {
    name: "read more button",
  });
  cleanup();
});

/*
Check if the text appears if the readmore button is clicked */
test("Check if the text appears if the readmore button is clicked", async () => {
  global.window.innerWidth = 1024;
  const user = userEvent.setup();

  const container = await setup();
  const button = screen.getByRole("button", {
    name: /read more button/i,
  });
  await user.click(button);
  const textAppears = await screen.getByText(/dosageAssist/i);
  expect(textAppears).toBeInTheDocument;
  cleanup();
});

/*
Check if the extra text is not there if the read more button is not clicked */
test("Check if the text appears if the readmore button is clicked", async () => {
  global.window.innerWidth = 1024;
  const user = userEvent.setup();
  const container = await setup();
  const button = screen.getByRole("button", {
    name: /read more button/i,
  });
  const textAppears = await screen.queryByText(/dosageAssist/i);
  expect(textAppears).not.toBeInTheDocument;
  cleanup();
});

/* check if the home page is loaded if back button is clicked in the top right */
test("check if the home page is loaded if back button is clicked in the top right", async () => {
  global.window.innerWidth = 1024;
  jest.mock(
    "next/link",
    () =>
      ({ children }) =>
        children
  );
  const user = userEvent.setup();
  const container = await setup();
  const button = screen.getByRole("button", {
    name: /back button/i,
  });
  // await user.click(button);
  cleanup();
});

/*
check if the text appears in full if the screen width is
smaller than 768px
*/

/*
check if the left dot takes you back to the first image */
test("the right dot takes you to the end if not already there", async () => {
  global.window.innerWidth = 1024;
  const user = userEvent.setup();
  const container = await setup();
  const button = screen.getAllByRole("button", {
    name: /slide dot/i,
  });
  await user.click(button[2]);
  expect(button[2]).toBeDisabled();
  cleanup();
});

/* check if the right dot takes you to the last image */

/* check if the middle dot takes you to the middle image */

/* Product information should display */
test("The product information should display", async () => {
  const user = userEvent.setup();
  const container = await setup();
  const title = screen.getByText(/Product Information/i);
  const productCode = screen.getByText(/^Product Code: [0-9]+?$/i);
  expect(title).toBeInTheDocument();
  expect(productCode).toBeInTheDocument();
});

//

/* Product specification should display */

test("The product information should display", async () => {
  const user = userEvent.setup();
  const container = await setup();
  const title = screen.getByText(/Product Specification/i);
  const list = screen.getByRole("list");
  const listItem = screen.findAllByRole("listitem");
  expect(list).toBeInTheDocument;
  expect(listItem).toBeInTheDocument;
  expect(title).toBeInTheDocument;
});
