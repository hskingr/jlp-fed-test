import React, { Component } from "react";
import { render, screen } from "@testing-library/react";
import Home, { getServerSideProps } from "../pages/index.jsx";

test("Home renders", async () => {
  //GIVEN
  // a user wants to access the home page
  const response = await getServerSideProps();
  //WHEN
  // the page loads
  const { container } = render(<Home data={response.props.data} />);
  //THEN
  // display the results
  const heading = screen.getByRole("heading", {
    name: "Dishwashers (24)",
  });
  expect(heading).toBeInTheDocument();
});

test("20 items are returned", async () => {});
